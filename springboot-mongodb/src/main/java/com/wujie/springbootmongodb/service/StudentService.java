package com.wujie.springbootmongodb.service;

import com.mongodb.client.result.UpdateResult;
import com.wujie.springbootmongodb.pojo.Student;

import java.util.List;

public interface StudentService {
    public Student add (Student student);
    public UpdateResult update (Student student);
    public void delete(String id);
    public Student findByName(String name);
    public List<Student> findBySex(String sex);
}
