package com.wujie.springbootmongodb.dao;

import com.wujie.springbootmongodb.pojo.Student;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface StudentRepository extends MongoRepository<Student,String> {
    public Student findByName(String name);
    public List<Student> findBySex(String sex);
}
