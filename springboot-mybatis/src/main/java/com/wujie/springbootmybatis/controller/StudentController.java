package com.wujie.springbootmybatis.controller;

import com.wujie.springbootmybatis.pojo.StudentEntity;
import com.wujie.springbootmybatis.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("student")
public class StudentController {
    @Autowired
    private StudentService studentService;
    @RequestMapping("add")
    public String add(StudentEntity studentEntity){
        Integer i = studentService.insertSelective(studentEntity);
        if (i != 1) {
            return "add fail";
        }else{
            return "add success";
        }
    }

    @RequestMapping("update")
    public String update(StudentEntity studentEntity){
        Integer i = studentService.updateByPrimaryKeySelective(studentEntity);
        if (i != 1) {
            return "update fail";
        }else{
            return "update success";
        }
    }

    @RequestMapping("list")
    public Object list(Integer id){
        if(id != null){
            StudentEntity studentEntity = studentService.selectByPrimaryKey(id);
            if(studentEntity != null){
                return studentEntity;
            }else {
                return " this id is not data";
            }
        }else {
            List<StudentEntity> aLl = studentService.findALl();
            if (aLl != null && aLl.size() > 0) {
                return aLl;
            }else {
                return "data is null";
            }
        }

    }

    @RequestMapping("delete")
    public String delete(Integer id){
        Integer i = studentService.deleteByPrimaryKey(id);
        if (i != 1) {
            return "delete fail";
        }else {
            return "delete success";
        }
    }
}
