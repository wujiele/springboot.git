package com.wujie.springbootmybatis.service.impl;

import com.wujie.springbootmybatis.dao.StudentDao;
import com.wujie.springbootmybatis.pojo.StudentEntity;
import com.wujie.springbootmybatis.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentDao studentDao;
    @Override
    public int deleteByPrimaryKey(Integer id) {
        return studentDao.deleteByPrimaryKey(id);
    }

    @Override
    public int insertSelective(StudentEntity record) {
        return studentDao.insertSelective(record);
    }

    @Override
    public StudentEntity selectByPrimaryKey(Integer id) {
        return studentDao.selectByPrimaryKey(id);
    }

    @Override
    public List<StudentEntity> findALl() {
        return studentDao.findAll();
    }

    @Override
    public int updateByPrimaryKeySelective(StudentEntity record) {
        return studentDao.updateByPrimaryKeySelective(record);
    }
}
