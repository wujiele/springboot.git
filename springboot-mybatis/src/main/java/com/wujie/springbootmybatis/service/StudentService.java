package com.wujie.springbootmybatis.service;

import com.wujie.springbootmybatis.pojo.StudentEntity;

import java.util.List;

public interface StudentService {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(StudentEntity record);

    StudentEntity selectByPrimaryKey(Integer id);

    List<StudentEntity> findALl();

    int updateByPrimaryKeySelective(StudentEntity record);

}
