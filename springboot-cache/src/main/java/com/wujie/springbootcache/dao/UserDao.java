/*
 * UserDao.java
 * Copyright(C) 贵州车联邦网络科技有限公司
 * All rights reserved.
 * -------------------------------
 * 2018-07-29 23:51:25生成
 */
package com.wujie.springbootcache.dao;

import com.wujie.springbootcache.pojo.UserBean;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDao {
    int deleteByPrimaryKey(Integer id);

    int insert(UserBean record);

    int insertSelective(UserBean record);

    UserBean selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserBean record);

    int updateByPrimaryKey(UserBean record);
}