package com.wujie.springbootcache.service;

import com.wujie.springbootcache.pojo.UserBean;

public interface UserService {
    public UserBean findById(Integer id);
}
