package com.wujie.springbootcache.service.impl;

import com.wujie.springbootcache.dao.UserDao;
import com.wujie.springbootcache.pojo.UserBean;
import com.wujie.springbootcache.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Override
    @Cacheable("user")
    public UserBean findById(Integer id) {
        System.out.println("走数据库");
        return userDao.selectByPrimaryKey(id);
    }
}
