package com.wujie.springbootcache.controller;

import com.wujie.springbootcache.pojo.UserBean;
import com.wujie.springbootcache.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;
    @RequestMapping("findById")
    public UserBean findById(Integer id){
        return userService.findById(id);
    }
}
