/*
 * UserBean.java
 * Copyright(C) 贵州车联邦网络科技有限公司
 * All rights reserved.
 * -------------------------------
 * 2018-07-29 23:51:25生成
 */
package com.wujie.springbootcache.pojo;

/**
 * 数据库表user对应的实体对象
 * @author wujie 
 * @version 1.0 
 * 2018-07-29 23:51:25
 */
public class UserBean {
    private Integer id;

    /**
     * 用户名
     */
    private String name;

    /**
     * 密码
     */
    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    @Override
    public String toString() {
        return "UserBean{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}