package com.wujie.springbootredis.controller;

import com.wujie.springbootredis.pojo.Student;
import com.wujie.springbootredis.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("student")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @RequestMapping("setString")
    public String setString(String key, String value) {
        studentService.setString(key, value);
        return "success";
    }
    @RequestMapping("getString")
    public String getString(String key) {
        String string = studentService.getString(key);
        if (string != null) {
            return string;
        }else {
            return "no data";
        }
    }
    @RequestMapping("setListString")
    public String setListString(String key, String... str) {
        Long aLong = studentService.setListString(key, str);
        if (aLong != null) {
            return "success "+aLong+" data";
        }else{
            return "fail";
        }
    }
    @RequestMapping("getListString")
    public List<String> getListString(String key, Integer start, Integer end) {
        List<String> listString = studentService.getListString(key, start, end);
        if (listString != null && listString.size() != 0) {
            return listString;
        }else{
            return null;
        }
    }
    @RequestMapping("add")
    public int addStudent(Student student) {
        return studentService.addStudent(student);
    }

    @RequestMapping("findById")
    public Student findById(Integer id) {
        Student student = studentService.findById(id);
        return student;
    }
    @RequestMapping("findAll")
    public List<Object> findAll(String key){
        List<Object> list = studentService.findAll(key);
        return list;
    }
}
