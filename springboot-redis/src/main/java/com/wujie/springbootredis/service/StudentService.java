package com.wujie.springbootredis.service;

import com.wujie.springbootredis.pojo.Student;

import java.util.List;

public interface StudentService {
    public void setString(String key,String value);

    public String getString(String key);

    public Long setListString(String key,String... str);

    public List<String> getListString(String key,Integer start,Integer end);

    public int addStudent(Student student);

    public Student findById(Integer id);

    public List<Object> findAll(String key);
}
