package com.wujie.springbootredis.service.impl;

import com.wujie.springbootredis.dao.RedisDao;
import com.wujie.springbootredis.pojo.Student;
import com.wujie.springbootredis.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private RedisDao redisDao;
    @Resource
    private RedisTemplate<String,Student> template;
    @Value("${student}")
    private String student;

    @Override
    public void setString(String key, String value) {
        redisDao.setKey(key, value);
    }

    @Override
    public String getString(String key) {
        return redisDao.getValue(key);
    }

    @Override
    public Long setListString(String key, String... str) {
        return redisDao.setList(key,str);
    }

    @Override
    public List<String> getListString(String key, Integer start, Integer end) {
        return redisDao.getList(key, start, end);
    }

    @Override
    public int addStudent(Student student) {
//        template.opsForValue().set(this.student + student.getId(), student, 600, TimeUnit.SECONDS);
        template.opsForHash().put(this.student,this.student+student.getId(),student);
        return 0;
    }


    @Override
    public Student findById(Integer id) {
        Student student = (Student) template.opsForHash().get(this.student, this.student + id);
        return student;
    }

    @Override
    public List<Object> findAll(String key) {
        List<Object> list = template.opsForHash().values(this.student);
        return  list;
    }
}
