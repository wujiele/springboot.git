package com.wujie.springbootredis.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Repository
public class RedisDao {
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private RedisTemplate<Object,Object> template;

    public void setKey(String key,String value){
        ValueOperations<String,String> operations = redisTemplate.opsForValue();
        //设置过期时间为一分钟
        operations.set(key,value,1, TimeUnit.MINUTES);
    }

    public String getValue(String key){
        ValueOperations<String,String> operations = redisTemplate.opsForValue();
        return  operations.get(key);
    }

    public Long setList(String key,String ...str){
        ListOperations<String, String> operations = redisTemplate.opsForList();
        Long student = operations.leftPushAll(key, str);
        return student;
    }

    public List<String> getList(String key,Integer start,Integer end){
        ListOperations<String, String> operations = redisTemplate.opsForList();
        List<String> list = operations.range(key, start, end);
        return list;
    }

    public Long addObject(Object object){
        SetOperations<Object, Object> operations = template.opsForSet();
        Long add = operations.add(object.getClass(), object);
        return add;
    }
}
