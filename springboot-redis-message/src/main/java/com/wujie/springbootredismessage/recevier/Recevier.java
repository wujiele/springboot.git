package com.wujie.springbootredisrecevier.recevier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.CountDownLatch;

public class Recevier {
    private static Logger logger = LoggerFactory.getLogger(Recevier.class);

    private CountDownLatch latch;

    @Autowired
    public Recevier(CountDownLatch latch) {
        this.latch = latch;
    }

    public void receiveMessage(String message) {
        System.out.println("Received <" + message + ">");
        logger.info("Received <" + message + ">");
        latch.countDown();
    }
}
