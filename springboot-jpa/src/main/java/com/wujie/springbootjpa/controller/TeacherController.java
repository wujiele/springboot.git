package com.wujie.springbootjpa.controller;

import com.wujie.springbootjpa.pojo.Teacher;
import com.wujie.springbootjpa.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("teacher")
public class TeacherController {
    @Autowired
    private TeacherService teacherService;

    @RequestMapping(value = "add")
    public String addTeacher(Teacher teacher){
        Teacher teacher1 = teacherService.add(teacher);
        return teacher1.toString();
    }

    @RequestMapping(value = "list")
    public Object list(@RequestParam("id") Integer id){
        if(id == null){
            return teacherService.findTeacher();
        }else {
            return teacherService.findTeacherById(id).toString();
        }
    }

    @RequestMapping("update")
    public String updateTeacher(Teacher teacher){
        Teacher teacher1 = teacherService.update(teacher);
        return teacher1.toString();
    }
}
