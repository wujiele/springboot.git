package com.wujie.springbootjpa.service.impl;

import com.wujie.springbootjpa.dao.TeacherDao;
import com.wujie.springbootjpa.pojo.Teacher;
import com.wujie.springbootjpa.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TeacherServiceImpl implements TeacherService {
    @Autowired
    private TeacherDao teacherDao;
    @Override
    public Teacher add(Teacher teacher) {
        Teacher save = teacherDao.save(teacher);
        return save;
    }

    @Override
    public Teacher update(Teacher teacher) {
        return teacherDao.saveAndFlush(teacher);
    }

    @Override
    public int delete(Integer id) {
        teacherDao.deleteById(id);
        return 0;
    }

    @Override
    public Teacher findTeacherById(Integer id) {
        Optional<Teacher> teacher = teacherDao.findById(id);
        if (teacher != null) {
            return teacher.get();
        }
        return null;
    }

    @Override
    public List<Teacher> findTeacher() {
        return teacherDao.findAll();
    }
}
