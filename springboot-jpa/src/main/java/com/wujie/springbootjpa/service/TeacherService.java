package com.wujie.springbootjpa.service;

import com.wujie.springbootjpa.pojo.Teacher;

import java.util.List;

public interface TeacherService {
    Teacher add (Teacher teacher);
    Teacher update(Teacher teacher);
    int delete(Integer id);
    Teacher findTeacherById(Integer id);
    List<Teacher> findTeacher();
}
