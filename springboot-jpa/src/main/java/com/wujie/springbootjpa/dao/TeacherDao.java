package com.wujie.springbootjpa.dao;

import com.wujie.springbootjpa.pojo.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherDao extends JpaRepository<Teacher,Integer> {

}
