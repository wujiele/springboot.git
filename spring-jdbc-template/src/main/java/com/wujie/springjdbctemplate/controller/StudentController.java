package com.wujie.springjdbctemplate.controller;

import com.wujie.springjdbctemplate.pojo.Student;
import com.wujie.springjdbctemplate.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("student")
public class StudentController {
    @Autowired
    private StudentService studentService;
    @RequestMapping("add")
    public String addStudent(Student student){
        int t = studentService.add(student);
        if (t == 1){
            return student.toString();
        }else {
            return "增加失败";
        }
    }

    @RequestMapping("update/{id}")
    public String update(Student student,@PathVariable("id") Integer id){
        /*Student student = new Student();
        student.setAge(age);
        student.setName(name);*/
        student.setId(id);
        int t = studentService.update(student);
        if(t == 1){
            return student.toString();
        }else {
            return "更新失败";
        }
    }

    @RequestMapping("{id}")
    public Student getStudentById(@PathVariable("id") Integer id){
        Student student = studentService.findStudentById(id);
        if(student != null){
            return student;
        }else {
            return null;
        }
    }

    @RequestMapping("list")
    public List<Student> getStudent(){
        return studentService.findStudent();
    }
}
