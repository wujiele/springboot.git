package com.wujie.springjdbctemplate.service;

import com.wujie.springjdbctemplate.dao.StudentDao;
import com.wujie.springjdbctemplate.pojo.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

public interface StudentService {
    int add (Student student);
    int update(Student student);
    int delete(Integer id);
    Student findStudentById(Integer id);
    List<Student> findStudent();


}
