package com.wujie.springjdbctemplate.service.impl;

import com.wujie.springjdbctemplate.dao.StudentDao;
import com.wujie.springjdbctemplate.pojo.Student;
import com.wujie.springjdbctemplate.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentDao studentDao;
    @Override
    public int add(Student student) {
        return studentDao.add(student);
    }

    @Override
    public int update(Student student) {
        return studentDao.update(student);
    }

    @Override
    public int delete(Integer id) {
        return studentDao.delete(id);
    }

    @Override
    public Student findStudentById(Integer id) {
        return studentDao.findStudentById(id);
    }

    @Override
    public List<Student> findStudent() {
        return studentDao.findStudent();
    }
}