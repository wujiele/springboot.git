package com.wujie.springjdbctemplate.dao;

import com.wujie.springjdbctemplate.pojo.Student;

import java.util.List;

public interface StudentDao {
    int add (Student student);
    int update(Student student);
    int delete(Integer id);
    Student findStudentById(Integer id);
    List<Student> findStudent();
}
