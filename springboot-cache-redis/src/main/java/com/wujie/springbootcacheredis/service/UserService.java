package com.wujie.springbootcacheredis.service;

import com.baomidou.mybatisplus.service.IService;
import com.wujie.springbootcacheredis.pojo.User;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wujie
 * @since 2018-07-25
 */
public interface UserService extends IService<User> {
    public List<?> selectUserPage(String name, Integer pageSize, Integer startPage);
    List<?> selectUserAll(Integer pageSize, Integer startPage);
    public boolean insertOrUpdate(User entity);
}
