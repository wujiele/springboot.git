package com.wujie.springbootcacheredis.service.impl;

import com.baomidou.mybatisplus.entity.TableInfo;
import com.baomidou.mybatisplus.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.toolkit.ReflectionKit;
import com.baomidou.mybatisplus.toolkit.StringUtils;
import com.baomidou.mybatisplus.toolkit.TableInfoHelper;
import com.wujie.springbootcacheredis.pojo.User;
import com.wujie.springbootcacheredis.mapper.UserMapper;
import com.wujie.springbootcacheredis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wujie
 * @since 2018-07-25
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Resource
    private RedisTemplate<String,User> template;
    @Resource
    private StringRedisTemplate redisTemplate;
    @Override
    public List<?> selectUserPage(String name, Integer pageSize, Integer startPage) {

        User user1 = (User) template.opsForHash().get("user", "user:" + name);
        List<User> userList = new ArrayList<User>();
//        List<Object> values = template.opsForHash().values("user:" + name);
        if(user1 != null){
            userList.add(user1);
            System.out.println("未走数据库");
            return userList;
        }
        List<User> users = userMapper.selectPage(new Page<User>(startPage,pageSize),new EntityWrapper<User>().like("username", name));
        for (User user:users

             ) {
            template.opsForHash().put("user","user:"+name,user);
//            redisTemplate.opsForHash().put("user","user:"+name,JSONObject.toJSON(user).toString());
            System.out.println("存入缓存");
        }
        return users;
    }

    @Override
    public List<?> selectUserAll(Integer pageSize, Integer startPage) {
        List<Object> list = template.opsForHash().values("user");
        if(list != null && list.size()>0){
            System.out.println("未走数据库");
            return list;
        }
        List<User> users = userMapper.selectPage(new Page<User>(startPage, pageSize), new EntityWrapper<User>());
        for (User user: users
             ) {
            template.opsForValue().set("user:"+user.getUsername(),user);
            System.out.println("走数据库");
        }
        return users;
    }

    /**
     * <p>
     * TableId 注解存在更新记录，否插入一条记录
     * </p>
     *
     * @param entity 实体对象
     * @return boolean
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean insertOrUpdate(User entity) {
        if (null != entity) {
            Class<?> cls = entity.getClass();
            TableInfo tableInfo = TableInfoHelper.getTableInfo(cls);
            if (null != tableInfo && StringUtils.isNotEmpty(tableInfo.getKeyProperty())) {
                Object idVal = ReflectionKit.getMethodValue(cls, entity, tableInfo.getKeyProperty());
                if (StringUtils.checkValNull(idVal)) {
                    return insert(entity);
                } else {
                    /*
                     * 更新成功直接返回，失败执行插入逻辑
                     */
                    template.opsForHash().put("user","user:"+entity.getUsername(),entity);
//                    template.opsForList().leftPush("user:"+user.getUsername(),user);
                    return updateById(entity) || insert(entity);
                }
            } else {
                throw new MybatisPlusException("Error:  Can not execute. Could not find @TableId.");
            }
        }
        return false;
    }
}
