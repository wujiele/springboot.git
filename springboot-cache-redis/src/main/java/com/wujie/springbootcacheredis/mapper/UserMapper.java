package com.wujie.springbootcacheredis.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.wujie.springbootcacheredis.pojo.User;
import org.apache.ibatis.annotations.Mapper;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wujie
 * @since 2018-07-25
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
