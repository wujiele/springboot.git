package com.wujie.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sun.security.krb5.Config;

@RestController
public class HelloController {
    @Value("${hello.name}")
    String helloName;
    @RequestMapping("hello/{name}")
    public String hello(@PathVariable("name") String name){
        return name+":"+helloName;
    }
    @Autowired
    ConfigBean configBean;

    @RequestMapping("java/bean")
    public ConfigBean configBean(){
        return configBean;
    }
}
