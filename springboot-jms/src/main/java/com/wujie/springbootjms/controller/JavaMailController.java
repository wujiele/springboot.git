package com.wujie.springbootjms.controller;

import com.wujie.springbootjms.mail.JavaMailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("mail")
public class JavaMailController {
    @Autowired
    private JavaMailUtil javaMailUtil;

    @RequestMapping("sendTxt")
    public String sendTxt(String title,String content,String from,String[] to ){
        String sendTxtMail = javaMailUtil.sendTxtMail(title, content, from, to);
        return sendTxtMail;
    }

    @RequestMapping("sendHtml")
    public String sendHtml(String title,String content,String from,String[] to ){
        String sendHtmlMail = javaMailUtil.sendHtmlMail(title, content, from, to);
        return sendHtmlMail;
    }

    @RequestMapping("sendImg")
    public String sendImg(String title,String content,String from,String[] to ){
        String sendAttachedImageMail = javaMailUtil.sendAttachedImageMail(title, content, from, to);
        return sendAttachedImageMail;
    }
    @RequestMapping("sendFile")
    public String sendFile(String title,String content,String from,String[] to ){
        String sendAttendedFileMail = javaMailUtil.sendAttendedFileMail(title, content, from, to);
        return sendAttendedFileMail;
    }
}
