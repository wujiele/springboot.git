package com.wujie.springbootswagger.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
@Api(description = "swagger测试")
@RestController
public class HelloController {
    @ApiOperation(value = "test1",notes = "测试swagger")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name",value="姓名",required = true,dataType = "string"),
            @ApiImplicitParam(name = "sex",value = "性别",required = true,dataType = "string")})
    @RequestMapping(value = "test1",method = RequestMethod.POST)
    public String test1(@RequestParam String name ,@RequestParam String sex){
        return name+" : "+sex;
    }
}
